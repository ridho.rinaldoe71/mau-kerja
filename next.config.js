// build configuration for load images and fonts
const withImages = require('next-images')
const withFonts = require('next-fonts')

module.exports = withFonts({
    webpack(config, options) {
        return config;
    }
})
module.exports = withImages()