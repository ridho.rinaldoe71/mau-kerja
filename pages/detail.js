import React from 'react'

// import component from library semantic-ui-react
import {
    Grid, Segment, Button, Image,
    Icon, Container, Label, Divider, List
} from 'semantic-ui-react'

// import local component with name is header
import Header from '../components/Header'

export default class Detail extends React.Component {

    // get all props variable with async function
    static async getInitialProps({ query }) {
        return query
    }

    constructor(props) {
        super(props)
        // declare state variable
        this.state = {
            data: [],
            detailIndex: null
        }
    }

    // component willmount will be rendered first when load the page
    componentWillMount = async () => {
        let { id } = this.props // get variable from props
        let response = null // build variable for data response from API

        // first load will get data (list jobs) from API using async function
        await fetch('http://private-27298f-frontendtestmaukerja.apiary-mock.com/jobs', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                response = responseJson
            }).catch((e) => {
                response = 'error'
            })

        // set response and detail index from id (query url) to state
        this.setState({ data: response, detailIndex: id })
    }

    // method render will be rendered after componentWillMount
    render() {
        // get variable from state
        const { data, detailIndex } = this.state

        return (
            <div style={{ backgroundColor: 'whitesmoke', height: '100vh' }}>
                {/* declare component Header*/}
                <Header />
                {
                    detailIndex !== null // if index from id (query url) is null or undefined then will no showing detail job
                        ?
                        <Container text style={{ padding: '2% 0' }}>
                            <Segment style={{ padding: 0, borderRadius: '15px 15px 0px 0px' }}>
                                <Image src={require('../assets/Watson-store.png')} style={{ width: '100%' }} />
                                <div style={{ display: 'flex', padding: '1em 1em 0' }}>
                                    <Image src={require('../assets/Watsons.png')} style={{ height: 58 }} />
                                    <div style={{ display: 'grid', padding: '1%' }}>
                                        <div style={{ display: 'grid', height: 45 }}>
                                            <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'larger', fontWeight: 600, alignSelf: 'center' }}>
                                                {data[detailIndex].company}
                                            </Label>
                                            <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal', alignSelf: 'center' }}>
                                                {data[detailIndex].title}
                                            </Label>
                                        </div>
                                    </div>
                                </div>

                                <Grid container columns={2} stackable style={{ margin: 0, padding: '0 11.5% 2%' }}>
                                    <Grid.Column style={{ padding: 0 }}>
                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                            <Icon name='money' />
                                            <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                                {'RM 4.000 - RM 5.000'}
                                            </Label>
                                        </Segment>
                                    </Grid.Column>

                                    <Grid.Column style={{ padding: 0 }}>
                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                            <Icon name='clock' />
                                            <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                                {data[detailIndex].type}
                                            </Label>
                                        </Segment>
                                    </Grid.Column>

                                    <Grid.Column style={{ padding: 0 }}>
                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                            <Icon name='map marker' />
                                            <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                                {data[detailIndex].location}
                                            </Label>
                                        </Segment>
                                    </Grid.Column>

                                    <Grid.Column style={{ padding: 0 }}>
                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                            <Icon name='share alternate' />
                                            <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                                {'Retail'}
                                            </Label>
                                        </Segment>
                                    </Grid.Column>
                                </Grid>

                                <Divider style={{ margin: 0 }} />
                                <Grid container columns={2} stackable style={{ margin: 0 }}>
                                    <Grid.Column style={{ padding: 0, textAlign: 'center' }}>
                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                            <Button type='submit' style={{ background: 'none' }}>
                                                <div style={{ display: 'flex' }}>
                                                    <Image src={require('../assets/Path.png')} />
                                                    <div style={{ padding: '0px 5px' }}>{'Save'}</div>
                                                </div>
                                            </Button>
                                        </Segment>
                                    </Grid.Column>

                                    <Grid.Column style={{ padding: 0, textAlign: 'center' }}>
                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                            <Button type='submit' style={{ background: 'none' }}>
                                                <div style={{ display: 'flex' }}>
                                                    <Image src={require('../assets/Shape.png')} />
                                                    <div style={{ padding: '0px 5px' }}>{'Apply'}</div>
                                                </div>
                                            </Button>
                                        </Segment>
                                    </Grid.Column>
                                </Grid>
                            </Segment>

                            <Segment>
                                <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'larger', fontWeight: 600, alignSelf: 'center' }}>
                                    {'Requirements'}
                                </Label>
                                <Divider />

                                <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 600, alignSelf: 'center' }}>
                                    {'Nationality : '}
                                </Label>
                                <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal', alignSelf: 'center' }}>
                                    {
                                        // mapping data of language
                                        data[detailIndex].requirements.language.map((lang, ind) => {
                                            let space = ind !== (data[detailIndex].requirements.language.length - 1) ? ', ' : ''
                                            return `${lang}${space}`
                                        })
                                    }
                                </Label>

                                <List>
                                    {
                                        // mapping data of requirements
                                        data[detailIndex].requirements.items.map((item, ind) => {
                                            return (
                                                <List.Item key={ind} style={{ display: 'flex' }}>
                                                    <Icon name='circle' size='tiny' style={{ alignSelf: 'center' }} />
                                                    <Label style={{ padding: '0 0 0 5px', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal', alignSelf: 'center' }}>
                                                        {item}
                                                    </Label>
                                                </List.Item>
                                            )
                                        })
                                    }
                                </List>
                            </Segment>

                            <Segment>
                                <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'larger', fontWeight: 600, alignSelf: 'center' }}>
                                    {'Responsibilities'}
                                </Label>
                                <Divider />

                                <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 600, alignSelf: 'center' }}>
                                    {`Maintain a professional level of customer service in promoting the companys's products:`}
                                </Label>

                                <List>
                                    {
                                        // mapping data of responsibilites
                                        data[detailIndex].responsibilites.map((res, ind) => {
                                            return (
                                                <List.Item key={ind} style={{ display: 'flex' }}>
                                                    <Icon name='circle' size='tiny' style={{ alignSelf: 'center' }} />
                                                    <Label style={{ padding: '0 0 0 5px', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal', alignSelf: 'center' }}>
                                                        {res}
                                                    </Label>
                                                </List.Item>
                                            )
                                        })
                                    }
                                </List>
                            </Segment>
                        </Container>
                        : ''
                }
            </div>
        )
    }
}