import React from 'react'

// import component from library semantic-ui-react
import {
    Grid, Segment, Button, Image,
    Icon, Container, Label, Divider
} from 'semantic-ui-react'
import Link from 'next/link'

// import local component with name is header
import Header from '../components/Header'

export default class Search extends React.Component {

    // get all props variable with async function
    static async getInitialProps(props) {
        return props
    }

    constructor(props) {
        super(props)
        // declare state variable
        this.state = {
            data: [],
            search: []
        }
    }

    // component willmount will be rendered first when load the page
    componentWillMount = async () => {
        let { query } = this.props // get variable from props
        let response = null // build variable for data response from API

        // first load will get data (list jobs) from API using async function
        await fetch('http://private-27298f-frontendtestmaukerja.apiary-mock.com/jobs', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => response.json())
            .then((responseJson) => {
                response = responseJson
            }).catch((e) => {
                response = 'error'
            })

        // getting the correct data based on search filter and save to result variable
        let result = this.loadData(response, query)

        // set response and result to state
        this.setState({ data: response, search: result })
    }

    // shouldComponentUpdate will be rendered when there is a change in the state or props
    shouldComponentUpdate(nextProps, nextState) {
        const { asPath } = this.props

        // condition when there is a change in object asPath
        if (nextProps.asPath !== asPath) {
            // getting the correct data based on search filter and save to result variable
            let result = this.loadData(nextState.data, nextProps.query)

            // set the new result search filter to state
            this.setState({ search: result })
        }

        return true
    }

    // method of filtering data (response API) based on search filter
    loadData(response, query) {
        let result = []
        let filter = [false, false, false, false]
        if (response.length !== 0) {
            response.map(item => {
                let company = item.company.toLowerCase()
                let title = item.title.toLowerCase()
                let location = item.location.toLowerCase()

                if (query.company !== '') {
                    filter[0] = true
                    filter[1] = true
                }
                if (query.location !== '') {
                    filter[2] = true
                }
                if (query.minSalary !== '') {
                    filter[4] = true
                }

                let getInd = []
                filter.map((item, index) => {
                    if (item) {
                        getInd = [...getInd, index]
                    }
                })

                let check = true
                for (let i = 0; i < getInd.length; i++) {
                    if (getInd[i] === 0) {
                        if (company.search(query.company.toLowerCase()) === -1) {
                            if (title.search(query.title.toLowerCase()) === -1) {
                                check = false
                                break
                            }
                        }
                    } else if (getInd[i] === 2) {
                        if (location.search(query.location.toLowerCase()) === -1) {
                            check = false
                            break
                        }
                    } else if (getInd[i] === 4) {
                        if (query.minSalary === 'Above RM 5.000') {
                            check = false
                            break
                        }
                    }
                }

                if (check) {
                    result = [...result, item]
                }
            })
        }

        return result
    }

    // method render will be rendered after componentWillMount
    render() {
        // get variable from state
        const { search } = this.state

        return (
            <div style={{ backgroundColor: 'whitesmoke', height: '100vh' }}>
                {/* declare component Header*/}
                <Header />
                <Container text style={{ padding: '2% 0' }}>
                    {
                        search.length !== 0 // if result of filter search is no data then no showing list jobs 
                            ? search.map((item, index) => { // mapping list search variable
                                return (
                                    <Link href={`/detail?id=${index}`}>
                                        <Segment key={index}>
                                            <div style={{ cursor: 'pointer' }} >
                                                <div style={{ display: 'flex' }}>
                                                    <Image src={require('../assets/Watsons.png')} />
                                                    <div style={{ display: 'grid', padding: '1%' }}>
                                                        <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'larger', fontWeight: 600, alignSelf: 'center' }}>
                                                            {item.company}
                                                        </Label>
                                                        <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal', alignSelf: 'center' }}>
                                                            {item.title}
                                                        </Label>
                                                    </div>
                                                </div>

                                                <Grid container columns={2} stackable style={{ margin: '2% 0 0' }}>
                                                    <Grid.Column style={{ padding: 0 }}>
                                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                                            <Icon name='money' />
                                                            <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                                                {'RM 4.000 - RM 5.000'}
                                                            </Label>
                                                        </Segment>
                                                    </Grid.Column>

                                                    <Grid.Column style={{ padding: 0 }}>
                                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                                            <Icon name='clock' />
                                                            <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                                                {item.type}
                                                            </Label>
                                                        </Segment>
                                                    </Grid.Column>

                                                    <Grid.Column style={{ padding: 0 }}>
                                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                                            <Icon name='map marker' />
                                                            <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                                                {item.location}
                                                            </Label>
                                                        </Segment>
                                                    </Grid.Column>
                                                </Grid>

                                                <Grid container columns={1} stackable style={{ margin: 0 }}>
                                                    <Grid.Column style={{ padding: 0 }}>
                                                        <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                                            <Label style={{ padding: '2% 0', background: 'none', fontSize: 'inherit', fontWeight: 'normal' }}>
                                                                {item.description}
                                                            </Label>
                                                        </Segment>
                                                    </Grid.Column>
                                                </Grid>
                                            </div>

                                            <Divider style={{ margin: 0 }} />
                                            <Grid container columns={2} stackable style={{ margin: 0 }}>
                                                <Grid.Column style={{ padding: 0, textAlign: 'center' }}>
                                                    <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                                        <Button type='submit' style={{ background: 'none' }}>
                                                            <div style={{ display: 'flex' }}>
                                                                <Image src={require('../assets/Path.png')} />
                                                                <div style={{ padding: '0px 5px' }}>{'Save'}</div>
                                                            </div>
                                                        </Button>
                                                    </Segment>
                                                </Grid.Column>

                                                <Grid.Column style={{ padding: 0, textAlign: 'center' }}>
                                                    <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                                        <Button type='submit' style={{ background: 'none' }}>
                                                            <div style={{ display: 'flex' }}>
                                                                <Image src={require('../assets/Shape.png')} />
                                                                <div style={{ padding: '0px 5px' }}>{'Apply'}</div>
                                                            </div>
                                                        </Button>
                                                    </Segment>
                                                </Grid.Column>
                                            </Grid>
                                        </Segment>
                                    </Link>
                                )
                            })
                            :
                            <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal', alignSelf: 'center' }}>
                                {'Search Not Found'}
                            </Label>
                    }
                </Container>
            </div>
        )
    }
}