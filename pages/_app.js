// pages/_app.js
// load css file from semantic-ui-css
import 'semantic-ui-css/semantic.min.css'

export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}