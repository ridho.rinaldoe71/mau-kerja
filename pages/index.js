import React from 'react'

// import component from library semantic-ui-react
import {
  Grid, Segment, Button, Image,
  Icon, Container, Label, Divider
} from 'semantic-ui-react'
import Link from 'next/link'

// import local component with name is header
import Header from '../components/Header'

class IndexPage extends React.Component {

  constructor(props) {
    super(props)
    // declare state variable
    this.state = {
      data: []
    }
  }

  // component willmount will be rendered first when load the page
  componentWillMount = async () => {
    let response = null // build variable for data response from API

    // first load will get data (list jobs) from API using async function
    await fetch('http://private-27298f-frontendtestmaukerja.apiary-mock.com/jobs', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    }).then((response) => response.json())
      .then((responseJson) => {
        response = responseJson // build variable for data response from API with json format
      }).catch((e) => {
        response = 'error'
      })

    // set response to state
    this.setState({ data: response })
  }

  // method render will be rendered after componentWillMount
  render() {
    // get variable from state
    const { data } = this.state

    return (
      <div style={{ backgroundColor: 'whitesmoke', height: '100vh' }}>
        {/* declare component Header*/}
        <Header />
        <Container text style={{ padding: '2% 0' }}>
          {
            data.length !== 0 // if data (response) is no data then no showing list jobs 
              ? data.map((item, index) => { // mapping list data
                return (
                  <Link href={`/detail?id=${index}`}>
                    <Segment key={index}>
                      <div style={{ cursor: 'pointer' }}>
                        <div style={{ display: 'flex' }}>
                          <Image src={require('../assets/Watsons.png')} />
                          <div style={{ display: 'grid', padding: '1%' }}>
                            <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'larger', fontWeight: 600, alignSelf: 'center' }}>
                              {item.company}
                            </Label>
                            <Label style={{ padding: 0, background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal', alignSelf: 'center' }}>
                              {item.title}
                            </Label>
                          </div>
                        </div>

                        <Grid container columns={2} stackable style={{ margin: '2% 0 0' }}>
                          <Grid.Column style={{ padding: 0 }}>
                            <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                              <Icon name='money' />
                              <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                {'RM 4.000 - RM 5.000'}
                              </Label>
                            </Segment>
                          </Grid.Column>

                          <Grid.Column style={{ padding: 0 }}>
                            <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                              <Icon name='clock' />
                              <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                {item.type}
                              </Label>
                            </Segment>
                          </Grid.Column>

                          <Grid.Column style={{ padding: 0 }}>
                            <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                              <Icon name='map marker' />
                              <Label style={{ padding: '2% 0', background: 'none', color: 'black', fontSize: 'inherit', fontWeight: 'normal' }}>
                                {item.location}
                              </Label>
                            </Segment>
                          </Grid.Column>
                        </Grid>

                        <Grid container columns={1} stackable style={{ margin: 0 }}>
                          <Grid.Column style={{ padding: 0 }}>
                            <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                              <Label style={{ padding: '2% 0', background: 'none', fontSize: 'inherit', fontWeight: 'normal' }}>
                                {item.description}
                              </Label>
                            </Segment>
                          </Grid.Column>
                        </Grid>
                      </div>

                      <Divider style={{ margin: 0 }} />
                      <Grid container columns={2} stackable style={{ margin: 0 }}>
                        <Grid.Column style={{ padding: 0, textAlign: 'center' }}>
                          <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                            <Button type='submit' style={{ background: 'none' }}>
                              <div style={{ display: 'flex' }}>
                                <Image src={require('../assets/Path.png')} />
                                <div style={{ padding: '0px 5px' }}>{'Save'}</div>
                              </div>
                            </Button>
                          </Segment>
                        </Grid.Column>

                        <Grid.Column style={{ padding: 0, textAlign: 'center' }}>
                          <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                            <Button type='submit' style={{ background: 'none' }}>\
                              <div style={{ display: 'flex' }}>
                                <Image src={require('../assets/Shape.png')} />
                                <div style={{ padding: '0px 5px' }}>{'Apply'}</div>
                              </div>
                            </Button>
                          </Segment>
                        </Grid.Column>
                      </Grid>
                    </Segment>
                  </Link>
                )
              })
              : ''
          }
        </Container>
      </div >
    )
  }
}

export default IndexPage
