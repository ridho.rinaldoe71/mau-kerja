import React from 'react'

// import component from library semantic-ui-react
import {
    Menu, Form, Grid, Segment, Button, Image,
    Icon, Dropdown
} from 'semantic-ui-react'
import Link from 'next/link'

// declare data for dropdown min salary
let minSalary = [
    { key: 1, value: 1, text: 'Below RM 3.000' },
    { key: 2, value: 2, text: 'RM 3.000 - RM 5.000' },
    { key: 3, value: 3, text: 'Above RM 5.000' }
]

// export local component Header
export default class Header extends React.Component {

    constructor(props) {
        super(props)
        // declare state variable
        this.state = {
            activeMenu: 'alljobs',
            fields: {
                jobtitle: "",
                location: "",
                minSalary: ""
            }
        }
    }

    // click menu for change active menu header
    handleClickMenu = (e, { name }) => {
        this.setState({ activeMenu: name })
    }

    // change field to update field of filter search to state variable
    handleChangeFields = (param, evt) => {
        const { fields } = this.state
        let update = fields

        if (param === 'minSalary') { // change variable state min salary
            update.minSalary = evt.target.textContent
        } else if (param === 'jobtitle') { // change variable state min job title company
            update.jobtitle = evt.target.value
        } else if (param === 'location') { // change variable state min job title location
            update.location = evt.target.value
        }

        // set fields to state
        this.setState({ fields: update })
    }

    // method render will be rendered after componentWillMount
    render() {
        // get variable from state
        const { activeMenu, fields } = this.state

        return (
            <div>
                <div style={{ backgroundColor: '#ed3554' }}>
                    <Menu secondary className="container test" style={{ backgroundColor: '#ed3554' }}>
                        {/* Link for router */}
                        <Link href="/">
                            <Menu.Item style={{ padding: 0 }}>
                                <Image src={require('../assets/top-logo-lg.png')} style={{ height: 36, width: 'auto' }} />
                            </Menu.Item>
                        </Link>

                        {/* Link for router */}
                        <Link href="/">
                            <Menu.Item
                                name='alljobs'
                                active={activeMenu === 'alljobs'}
                                style={{ color: 'white' }}
                            >
                                {'All Jobs'}
                            </Menu.Item>
                        </Link>

                        <Menu.Item
                            name='tools'
                            active={activeMenu === 'tools'}
                            onClick={this.handleClickMenu}
                            style={{ color: 'white' }}>
                            {'Tools'}
                        </Menu.Item>

                        <Menu.Item
                            name='help'
                            active={activeMenu === 'help'}
                            onClick={this.handleClickMenu}
                            style={{ color: 'white' }}>
                            {'Help'}
                        </Menu.Item>

                        <Menu.Item
                            name='blog'
                            active={activeMenu === 'blog'}
                            onClick={this.handleClickMenu}
                            href={'https://blog.maukerja.my/'} // go to blog maukerja.my
                            style={{ color: 'white' }}>
                            {'Blog'}
                        </Menu.Item>
                    </Menu>
                </div>

                <div style={{ backgroundColor: 'white' }}>
                    <Form className="container">
                        <Grid container columns={4} stackable style={{ margin: 0 }}>
                            <Grid.Column>
                                <Segment style={{ padding: 0, borderRadius: 15, border: 'none', boxShadow: 'none' }}>
                                    <Form.Field >
                                        <input onChange={this.handleChangeFields.bind(this, 'jobtitle')} placeholder="Job Title Company" name='jobTitle' style={{ borderRadius: 15 }} />
                                    </Form.Field>
                                </Segment>
                            </Grid.Column>
                            <Grid.Column>
                                <Segment style={{ padding: 0, borderRadius: 15, border: 'none', boxShadow: 'none' }}>
                                    <Form.Field >
                                        <input onChange={this.handleChangeFields.bind(this, 'location')} placeholder="Location" name='location' style={{ borderRadius: 15 }} />
                                    </Form.Field>
                                </Segment>
                            </Grid.Column>
                            <Grid.Column>
                                <Segment style={{ padding: 0, border: 'none', boxShadow: 'none' }}>
                                    <Dropdown
                                        onChange={this.handleChangeFields.bind(this, 'minSalary')}
                                        name='minSalary'
                                        placeholder='Min Salary (MYR)'
                                        fluid
                                        search
                                        selection
                                        options={minSalary}
                                        style={{ borderRadius: 15 }}
                                    />
                                </Segment>
                            </Grid.Column>
                            <Grid.Column>
                                <Segment style={{ padding: 0, borderRadius: 15, border: 'none', boxShadow: 'none' }}>
                                    <Form.Field >
                                        <Link href={`/search?company=${fields.jobtitle}&title=${fields.jobtitle}&location=${fields.location}&minSalary=${fields.minSalary}`}>
                                            <Button type='submit' style={{ borderRadius: 15, paddingLeft: '20%', paddingRight: '20%', backgroundColor: '#ed3554', color: 'white', fontWeight: 'normal' }}>
                                                <Icon name='search' />{'Find Job'}
                                            </Button>
                                        </Link>
                                    </Form.Field>
                                </Segment>
                            </Grid.Column>
                        </Grid>
                    </Form>
                </div>
            </div>
        )
    }
}